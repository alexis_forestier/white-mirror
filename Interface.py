from tkinter import *
import time
import requests

# Fenetre et customisation ---------------------------------------------------------------------------------------#
window = Tk()

window.title("White Mirror")
window.iconbitmap("images/WM icon transparent.ico")
window.geometry("800x480")
window.minsize(800, 480)
#window.maxsize(800, 480)               #Retiré pour permettre le plein écran
window.config(background='dark blue')

# Image ----------------------------------------------------------------------------------------------------------#
image = PhotoImage(file='images/background2.gif')                                              #La taille de l'image de fond doit-être de la même taille que l'écran
exit_ = PhotoImage(file='images/exit_logo2.png').zoom(5).subsample(10)
ath_ = PhotoImage(file='images/ath_logo.png').zoom(5).subsample(10)
clear_sky = PhotoImage(file='images/clear_skyd.png').zoom(5).subsample(10)
few_clouds = PhotoImage(file='images/few_cloudsd.png').zoom(5).subsample(10)
scattered_clouds = PhotoImage(file='images/scattered_clouds.png').zoom(5).subsample(10)
broken_clouds = PhotoImage(file='images/broken_clouds.png').zoom(5).subsample(10)
shower_rain = PhotoImage(file='images/shower_rain.png').zoom(5).subsample(10)
rain = PhotoImage(file='images/raind.png').zoom(5).subsample(10)
thunderstorm = PhotoImage(file='images/thunderstorm.png').zoom(5).subsample(10)
snow = PhotoImage(file='images/snow.png').zoom(5).subsample(10)
mist = PhotoImage(file='images/mist.png').zoom(5).subsample(10)
error = PhotoImage(file='images/error.png').zoom(5).subsample(10)
power_off = PhotoImage(file='images/power_off.png').zoom(3).subsample(10)
power_on = PhotoImage(file='images/power_on.png').zoom(3).subsample(10)

# Background -----------------------------------------------------------------------------------------------------#
canvas = Canvas(window, width=800, height=480, bg='magenta', bd=10, relief=SOLID, highlightthickness=0)        #Taille de l'écran utilisé
canvas.create_image(0, 0, image=image, anchor=NW)
canvas.pack(expand=YES)


# Recherche des Données Meteo ------------------------------------------------------------------------------------#
api_address = 'http://api.openweathermap.org/data/2.5/weather?appid=0c42f7f6b53b244c78a418f4f181282a&q='
city = "Bourg-en-Bresse"  # input('City Name :')
url = api_address + city
json_data = requests.get(url).json()
weather_data = json_data['weather'][0]['description']                # Recupère les données de la meteo
temp_data = json_data['main']['temp']                                # Recupère les données de la température
tempmin_data = json_data['main']['temp_min']
tempmax_data = json_data['main']['temp_max']

tp = round(temp_data)
tp -= 273.15  # Mettre en celsius
celsius = round(tp)

tpmin = round(tempmin_data)
tpmin -= 273.15
celminus = round(tpmin)

tpmax = round(tempmax_data)
tpmax -= 273.15
celplus = round(tpmax)

###For Dev###
print(weather_data)
print(temp_data)
print(tp)
print(tempmin_data)
print(tpmin)
print(tempmax_data)
print(tpmax)
###For Dev###



# Listes ----------------------#
thunderstorm_list = ["thunderstorm with light rain", "thunderstorm with rain",
                     "	thunderstorm with heavy rain", "	light thunderstorm",
                     "thunderstorm", "heavy thunderstorm", "ragged thunderstorm",
                     "thunderstorm with light drizzle", "thunderstorm with drizzle",
                     "thunderstorm with heavy drizzle"]
drizzle_list = ["light intensity drizzle", "drizzle", "heavy intensity drizzle",
                "light intensity drizzle rain", "drizzle rain", "heavy intensity drizzle rain",
                "shower rain and drizzle", "heavy shower rain and drizzle", "shower drizzle",
                "freezing rain", "light intensity shower rain", "shower rain", "heavy intensity shower rain",
                "ragged shower rain"]
rain_list = ["light rain", "moderate rain", "heavy intensity rain", "very heavy rain", "extreme rain"]
snow_list = ["light snow", "Snow", "Heavy snow", "Sleet", "Light shower sleet", "Shower sleet", "Light rain and snow",
             "Rain and snow", "Light shower snow", "Shower snow", "Heavy shower snow"]
mist_list = ["mist", "Smoke", "Haze", "sand/ dust whirls", "fog", "sand", "dust", "volcanic ash", "squalls", "tornado"]
# -----------------------------#




def power():
    global menu_power
    try:
        menu_power.destroy()
        menu_power = None
        button_power.config(image=power_off)
    except:
        def ath():
            global clock, weather_frame, menu_power
            try:
                clock.destroy()
                clock = None
                weather_frame.destroy()
                weather_frame = None
                menu_power.destroy()
                menu_power = None

            except:
                # partie horloge ---------------------------------------------------------------------------------#
                def the_time():
                    current_time = time.strftime('%H:%M:%S')
                    clock_label['text'] = current_time
                    clock.after(1000, the_time)

                clock = Frame(canvas, width=80, height=48, bg='black')
                clock_label = Label(clock, font=("Arial", 32), bg='black', fg='white')
                clock_label.pack()
                clock.place(relx=0, rely=0, anchor=NW)

                the_time()

                # Partie meteo -----------------------------------------------------------------------------------#
                def weather_info():
                    global info_sup
                    try:
                        info_sup.destroy()
                        info_sup = None
                    except:
                        # Partie Meteo > Meteo et Temp. Simple > Icone Bouton > Info Sup. ------------------------#
                        info_sup = Frame(canvas, bg='black')
                        info_label = Label(info_sup, text="Weather: {}\n"
                                                          "- Description: {}\n\n"
                                                          "Temperature: {}°C\n"
                                                          "- temp.min: {}°C\n"
                                                          "- temp.max: {}°C\n\n"
                                                          "Pressure: {}hpa\n\n"
                                                          "Humidity: {}%".format(json_data['weather'][0]['main'],
                                                                                 json_data['weather'][0]['description'],
                                                                                 celsius, celminus, celplus,
                                                                                 json_data['main']['pressure'],
                                                                                 json_data['main']['humidity']),
                                           bg='black', fg='white', font=("Arial", 10))
                        info_label.pack(side=LEFT)
                        info_sup.place(relx=0, rely=0.5, anchor=W)

                menu_power.destroy()
                menu_power = None

                # Partie Meteo > Meteo et Temp. Simple -----------------------------------------------------------#
                weather_frame = Frame(canvas, width=80, height=30, bg='black')

                # Partie Meteo > Meteo et Temp. Simple > Icone Bouton --------------------------------------------#
                weather_icon = Button(weather_frame, bg='black', command=weather_info)
                weather_icon.pack(side=LEFT)

                # Partie Meteo > Meteo et Temp. Simple > Temp. ---------------------------------------------------#
                temp_label = Label(weather_frame, font=("Arial", 32), bg='black', fg='white')
                temp_label.pack(side=RIGHT)
                temp_label.config(text="{}°C".format(celsius))

                weather_frame.place(relx=0.5, rely=0, anchor=N)

                # Configuration de l'icone Meteo -----------------------------------------------------------------#
                if weather_data == "clear sky":
                    weather_icon.config(image=clear_sky)
                elif weather_data == "few clouds":
                    weather_icon.config(image=few_clouds)
                elif weather_data == "scattered clouds":
                    weather_icon.config(image=scattered_clouds)
                elif weather_data == "broken clouds" or "overcast clouds":
                    weather_icon.config(image=broken_clouds)
                elif weather_data in drizzle_list:
                    weather_icon.config(image=shower_rain)
                elif weather_data in rain_list:
                    weather_icon.config(image=rain)
                elif weather_data in thunderstorm_list:
                    weather_icon.config(image=thunderstorm)
                elif weather_data in snow_list:
                    weather_icon.config(image=snow)
                elif weather_data in mist_list:
                    weather_icon.config(image=mist)
                else:
                    print("error")
                    weather_icon.config(image=error)


        # Bouton Power > Menu Power ------------------------------------------------------------------------------#
        menu_power = Frame(canvas, bg='black')

        # Bouton Power > Menu Power > Bouton Affichage de l'ATH --------------------------------------------------#
        button_ath = Button(menu_power, image=ath_, width=40, height=40, bg='black', command=ath)
        button_ath.pack(side=LEFT)

        # Bouton Power > Menu Power > Bouton Quitter l'application -----------------------------------------------#
        button_quit = Button(menu_power, image=exit_, width=40, height=40, bg='black', command=window.quit)
        button_quit.pack(side=RIGHT)

        menu_power.place(relx=0.843, rely=0.658)

        button_power.config(image=power_on)


# Bouton Power ---------------------------------------------------------------------------------------------------#
button_power = Button(canvas, bg='black', image=power_off, command=power)
button_power.place(relx=0.84, rely=0.75)



# Plein Ecran ----------------------------------------------------------------------------------------------------#
window.attributes("-fullscreen", True)
window.bind("<F11>", lambda event: window.attributes("-fullscreen", not window.attributes(
    "-fullscreen")))  # F11 pour switch entre fullscreen ou non
window.bind("<Escape>", lambda event: window.attributes("-fullscreen", False))  # Esc pour quitter le mode pleine ecran

window.mainloop()