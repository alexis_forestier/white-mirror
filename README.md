# WHITE MIRROR

Projet de Terminale SIN. Analyse, conception et prototypage d'un miroir connecté.

Ce projet réparti sur toute l'année, consistait à concevoir le prototype d'un miroir connecté. Le prototype était un programme en python installé dans une carte Raspberry Pi 4 muni d'un écran et d'une caméra.

**Partie interface uniquement**.

En collaboration avec CHARDON Baptiste, CROZET Samuel

## Tester le projet

1. Cloner le projet 
2. Lancer le projet avec un IDE capable de compiler du Python (recommandation : PyCharm)
3. Faite **run** ou **debug**

## Informations du projet original

*le Projet original a été supprimé*.

| Temps de travail | +75 heures |
| Durée début/fin | 167 jours |
